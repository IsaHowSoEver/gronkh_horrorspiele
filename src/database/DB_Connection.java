package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DB_Connection {

	private String sUrl = "jdbc:mysql://localhost:3306/db_gronkh_liste";
	private Connection conn = null;
	
	public Connection getConnection(String sUser, String sPasswd) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");	
			conn = DriverManager.getConnection(sUrl ,sUser, sPasswd);
			System.out.println("Verbindung hergestellt.");			
		}catch(ClassNotFoundException ex) {
			ex.printStackTrace();		
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return conn;
	}

}
