package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DB_Query_Shooter {	
		private Connection conn;
		private ResultSet rs;
		private PreparedStatement ps;
		private String sQuery= "Select s_titel, s_folgen, s_beschreibung, s_bewertung FROM tbl_shooter";

		public void getConnection() {
			DB_Connection connection = new DB_Connection();
			conn = connection.getConnection("root", "");
		}

		public ArrayList<ArrayList<String>> getResult() {
			ArrayList<ArrayList<String>> result= new ArrayList<ArrayList<String>>();
			try {
				ps = conn.prepareStatement(sQuery);
				rs = ps.executeQuery();
				while(rs.next()) {
					ArrayList<String> row = new ArrayList<String>();
					row.add(rs.getString("s_titel"));
					row.add(rs.getString("s_folgen"));
					row.add(rs.getString("s_beschreibung"));
					row.add(rs.getString("s_bewertung"));
					result.add(row);		
				}
				ps.close();
				rs.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			return result;
		}

}
