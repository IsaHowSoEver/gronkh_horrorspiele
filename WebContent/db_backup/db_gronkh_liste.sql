-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 07. Okt 2021 um 10:25
-- Server-Version: 10.4.13-MariaDB
-- PHP-Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `db_gronkh_liste`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_atmosphaere`
--

CREATE TABLE `tbl_atmosphaere` (
  `a_id` int(11) NOT NULL,
  `a_titel` varchar(100) NOT NULL,
  `a_folgen` int(11) NOT NULL,
  `a_beschreibung` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_psycho`
--

CREATE TABLE `tbl_psycho` (
  `p_id` int(11) NOT NULL,
  `p_titel` varchar(100) NOT NULL,
  `p_folgen` int(11) NOT NULL,
  `p_beschreibung` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_shooter`
--

CREATE TABLE `tbl_shooter` (
  `s_id` int(11) NOT NULL,
  `s_titel` varchar(100) NOT NULL,
  `s_folgen` int(11) NOT NULL,
  `s_beschreibung` varchar(300) NOT NULL,
  `s_bewertung` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `tbl_shooter`
--

INSERT INTO `tbl_shooter` (`s_id`, `s_titel`, `s_folgen`, `s_beschreibung`, `s_bewertung`) VALUES
(1, 'Testspiel', 3, 'Testspiel-Beschreibung', 'Ganz gut'),
(2, 'Testspiel 2', 15, 'Testspiel-Beschreibung 2', 'Nicht mein Spiel. Zu wenig Jumpscares.');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `tbl_atmosphaere`
--
ALTER TABLE `tbl_atmosphaere`
  ADD PRIMARY KEY (`a_id`);

--
-- Indizes für die Tabelle `tbl_psycho`
--
ALTER TABLE `tbl_psycho`
  ADD PRIMARY KEY (`p_id`);

--
-- Indizes für die Tabelle `tbl_shooter`
--
ALTER TABLE `tbl_shooter`
  ADD PRIMARY KEY (`s_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `tbl_atmosphaere`
--
ALTER TABLE `tbl_atmosphaere`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `tbl_psycho`
--
ALTER TABLE `tbl_psycho`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `tbl_shooter`
--
ALTER TABLE `tbl_shooter`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
