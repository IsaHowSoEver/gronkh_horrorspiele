<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="database.*" %>
<%@ page import="java.util.*" %>
<% 
DB_Query_Shooter s_query = new DB_Query_Shooter(); 
s_query.getConnection();
ArrayList<ArrayList<String>> aResult = s_query.getResult();
for(int i=0; i<aResult.size(); i++){
	for(int j=0; j<aResult.get(i).size(); j++){
		System.out.println(aResult.get(i).get(j));
	}
}
%>
<!DOCTYPE html>
<html lang="de" class="h-100">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<title>Gronkh-Horrorspiele</title>
<meta http-equiv="X-UA-Compatible" content="IE=10" />
<link href="library/css/styles.css" rel="stylesheet">
</head>
<body class="d-flex flex-column h-100">
		<!-- Navigationsleiste -->
		<header>
		<!-- Fixed navbar -->
		<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
			<a class="navbar-brand" id="headline" href="#">Gronkh-Horrorspiele</a> 
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link">Shooter</a></li>
				<li class="nav-item"><a class="nav-link">Atomspähre</a></li>
				<li class="nav-item"><a class="nav-link">Psycho</a></li>
			</ul>
		</nav>
	</header>
	<main>
		<form>
			<p id="fehler"></p>
			<table id="dashboard" class="table">

			</table>
		</form>
	</main>
	<!-- Footer -->
	<footer class="py-5 bg-dark fixed-bottom">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright &copy; Gronkh-Horrorspiele 2021</p>
		</div>

	</footer>
	<script src="library/js/scripts.js"></script>
</body>

</html>
